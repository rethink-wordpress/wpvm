# WPVM

WordPress development on virtual machines or Docker containers

## Quick Start Guide

If you want to install a WordPress site locally with minimal fuss, just:

  1. Install [Vagrant](https://www.vagrantup.com/downloads.html) and [VirtualBox](https://www.virtualbox.org/wiki/Downloads).
  2. Download or clone this project to your workstation.
  3. `cd` into this project directory and run `vagrant up`.